const express = require("express");
const axios = require("axios");
const crypto = require("crypto");
const app = express();
const port = process.env.PORT || 3000;

app.get("/api/quote", async (req, res) => {
  try {
    const [exchangeRateRes, frankfurterRes] = await Promise.allSettled([
      axios.get(
        `https://api.exchangerate-api.com/v4/latest/${req.query.from_currency_code}`
      ),
      axios.get(
        `https://api.frankfurter.app/latest?from=${req.query.from_currency_code}&to=${req.query.to_currency_code}`
      ),
    ]);

    const exchangeRate = exchangeRateRes.value.data,
    frankfurter = frankfurterRes.value?.data;
    
    const amount = parseFloat(req.query.amount);
    const exchangeRateConversion =
      (exchangeRate.rates[req.query.to_currency_code] || 0) * amount;
    const frankfurterConversion =
      (frankfurter?.rates?.[req.query.to_currency_code] || 0) * amount;

    const id = crypto.randomBytes(16).toString("hex");

    const isGreater10000USD = exchangeRate.rates.USD * amount >= 10000;

    if (
      frankfurterConversion &&
      (exchangeRateConversion < frankfurterConversion || isGreater10000USD)
    ) {
      res.status(200).json({
        exchange_rate: frankfurter.rates[req.query.to_currency_code],
        id,
        amount: Math.round((frankfurterConversion + Number.EPSILON) * 100) / 100,
        provider_name: "Frankfurter",
      });
    } else {
      res.status(200).json({
        exchange_rate: exchangeRate.rates[req.query.to_currency_code],
        id,
        amount: Math.round((exchangeRateConversion + Number.EPSILON) * 100) / 100,
        provider_name: "ExchangeRate-API",
      });
    }
  } catch (error) {
    console.log(error);
    res.sendStatus(500);
  }
});

app.listen(port, () => {
  console.log(`Example app listening at port ${port}`);
});

process.on("uncaughtException", function (err) {
  console.error(new Date().toUTCString() + " uncaughtException:", err.message);
  console.error(err.stack);
});
